-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: proiect_sd
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `anomalous` bit(1) DEFAULT NULL,
  `end` varchar(200) NOT NULL,
  `label` varchar(200) NOT NULL,
  `patientid` int(11) NOT NULL,
  `recommendation` varchar(255) DEFAULT NULL,
  `start` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (1,_binary '\0','2011-11-28 14:27:07','Toileting	',4,NULL,'2011-11-28 14:22:38'),(2,_binary '','2011-11-28 15:04:00','Lunch',3,NULL,'2011-11-28 14:27:11'),(3,_binary '','2011-11-28 16:05:29','Grooming',3,'\"recomandare5\"','2011-11-28 15:04:59'),(4,_binary '\0','2011-11-28 20:20:00','Spare_Time/TV',4,NULL,'2011-11-28 15:07:01'),(5,_binary '\0','2011-11-28 20:20:59','Snack	',4,NULL,'2011-11-28 20:20:55'),(6,_binary '','2011-11-29 02:06:00','Spare_Time/TV',1,'\"recomandare1\"','2011-11-28 20:21:15'),(7,_binary '','2011-11-29 16:31:00','Sleeping',2,NULL,'2011-11-29 02:16:00'),(8,_binary '\0','2011-11-29 18:36:55','Toileting	',2,NULL,'2011-11-29 11:31:55'),(9,_binary '\0','2011-11-29 11:51:13','Showering	',3,NULL,'2011-11-29 11:49:57'),(10,_binary '\0','2011-11-29 12:50:00','Breakfast	',2,'\"rec3\"','2011-11-29 12:08:28'),(11,_binary '\0','2011-11-29 13:22:00','Grooming',3,NULL,'2011-11-29 12:19:01'),(12,_binary '\0','2011-11-29 15:24:59','Spare_Time/TV',1,'\"rec4\"','2011-11-29 12:22:38'),(13,_binary '\0','2011-11-29 16:25:32','Snack	',2,'\"rec2\"','2011-11-29 13:25:29'),(14,_binary '\0','2011-11-29 15:12:26','Spare_Time/TV',4,NULL,'2011-11-29 13:25:38'),(15,_binary '','2011-11-29 16:17:57','Toileting',3,NULL,'2011-11-29 05:13:28'),(16,_binary '\0','2011-11-29 15:45:54','Lunch',3,NULL,'2011-11-29 15:14:33'),(17,_binary '\0','2011-11-29 15:50:54','Grooming	',3,NULL,'2011-11-29 15:49:51'),(18,_binary '','2011-11-29 16:31:27','Toileting',1,NULL,'2011-11-29 06:18:00'),(19,_binary '\0','2011-11-29 17:08:07','Spare_Time/TV',3,NULL,'2011-11-29 16:34:17'),(20,_binary '\0','2011-11-29 17:09:29','Toileting	',4,NULL,'2011-11-29 17:08:58'),(21,_binary '\0','2011-11-29 18:57:22','Spare_Time/TV',3,NULL,'2011-11-29 17:43:00'),(22,_binary '','2011-11-29 22:23:38','Leaving',4,NULL,'2011-11-29 09:02:15');
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication`
--

DROP TABLE IF EXISTS `medication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dosage` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `side_effects` varchar(200) DEFAULT NULL,
  `taken` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication`
--

LOCK TABLES `medication` WRITE;
/*!40000 ALTER TABLE `medication` DISABLE KEYS */;
INSERT INTO `medication` VALUES (1,10,'m1','nu',NULL),(2,10,'m2','nu',NULL),(3,10,'m3','nu',NULL),(4,10,'m4','nu',NULL);
/*!40000 ALTER TABLE `medication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_per_plan`
--

DROP TABLE IF EXISTS `medication_per_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_per_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intakemoment` varchar(200) DEFAULT NULL,
  `taken` bit(1) DEFAULT NULL,
  `medication_id` int(11) DEFAULT NULL,
  `medication_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgn2e42ex4j0duqnlu7njv8paj` (`medication_id`),
  KEY `FKfkce1m2eh0v32gp5hp5dxg09c` (`medication_plan_id`),
  CONSTRAINT `FKfkce1m2eh0v32gp5hp5dxg09c` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`id`),
  CONSTRAINT `FKgn2e42ex4j0duqnlu7njv8paj` FOREIGN KEY (`medication_id`) REFERENCES `medication` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_per_plan`
--

LOCK TABLES `medication_per_plan` WRITE;
/*!40000 ALTER TABLE `medication_per_plan` DISABLE KEYS */;
INSERT INTO `medication_per_plan` VALUES (1,'13:00 20:00',_binary '\0',1,2),(2,'20:00 05:00',_binary '',2,2),(3,'14:00 23:00',_binary '\0',3,3);
/*!40000 ALTER TABLE `medication_per_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_plan`
--

DROP TABLE IF EXISTS `medication_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `end` varchar(200) DEFAULT NULL,
  `start` varchar(200) DEFAULT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsbssmqwap7gwimwcnwmolheef` (`patient_id`),
  CONSTRAINT `FKsbssmqwap7gwimwcnwmolheef` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_plan`
--

LOCK TABLES `medication_plan` WRITE;
/*!40000 ALTER TABLE `medication_plan` DISABLE KEYS */;
INSERT INTO `medication_plan` VALUES (2,'20','12',1),(3,'25','20',1);
/*!40000 ALTER TABLE `medication_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medical_record` varchar(200) DEFAULT NULL,
  `caregiver_id` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6xsugcipalsc4u3i7db2oob99` (`caregiver_id`),
  KEY `FKs803llq8v2bbsy5sqmahjxc4j` (`doctor_id`),
  KEY `FKp6ttmfrxo2ejiunew4ov805uc` (`user_id`),
  CONSTRAINT `FK6xsugcipalsc4u3i7db2oob99` FOREIGN KEY (`caregiver_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKp6ttmfrxo2ejiunew4ov805uc` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKs803llq8v2bbsy5sqmahjxc4j` FOREIGN KEY (`doctor_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'nu',2,1,3),(2,'nu',2,1,4),(3,'nu',2,1,5);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `birthdate` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'cj','20mai1970','m','andrei','123',0,'doc1'),(2,'cj','19iun1987','m','manuel','123',1,'car1'),(3,'sebes','26 April 1997','male','Vlad dreghici','123',2,'vlad'),(4,'cj','18 iunie 2000','female','pacient2','123',2,'pacient2'),(5,'cj','20 iunie 2000','male','pacient3','123',2,'pacient3');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-28 21:19:28
