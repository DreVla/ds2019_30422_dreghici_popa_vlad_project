import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import patient.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    private static List<Patient> patients = new ArrayList<>();
    private static List<Activity> activities = new ArrayList<>();
    private static List<String> patientsNames = new ArrayList<>();

    private int warning;

    public static void main(String[] args) throws MalformedURLException {
        loadPatients();
        loadActivities();

        launch();
    }


    private static void loadPatients() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/patients?wsdl");

        PatientServiceImplService patientServiceImplService
                = new PatientServiceImplService(url);
        PatientServiceImpl patientService
                = patientServiceImplService.getPatientServiceImplPort();
        patients = patientService.getAllPatients();
        for(Patient p: patients){
            System.out.println(p.getName());
            patientsNames.add(p.getName());
        }
    }

    private static void loadActivities() throws MalformedURLException {
        URL url = new URL("http://localhost:8080/activity?wsdl");

        ActivityServiceImplService activityServiceImplService
                = new ActivityServiceImplService(url);
        ActivityServiceImpl activityService
                = activityServiceImplService.getActivityServiceImplPort();
        activities = activityService.getAllActivities();

    }


    @Override
    public void start(Stage stage) throws Exception {
        StackPane rootPane = new StackPane();
        Scene scene = new Scene(rootPane, 480, 640);
        stage.setScene(scene);

        Pane pane1 = new Pane();
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll(patientsNames);
        ListView<String> lv = new ListView<String>(items);
        lv.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Label warningLabel = new Label("Patient is ok");
                warningLabel.setTextFill(Color.web("#1e8100"));
                BorderPane secondaryLayout = new BorderPane();
//                secondaryLayout.getChildren().add(secondLabel);

                Scene secondScene = new Scene(secondaryLayout, 1920, 901);

                // New window (Stage)
                Stage newWindow = new Stage();
                newWindow.setTitle("Activity");
                newWindow.setScene(secondScene);

                // Specifies the modality for new window.
                newWindow.initModality(Modality.WINDOW_MODAL);

                // Specifies the owner Window (parent) for new window
                newWindow.initOwner(stage);

                // Set position of second window, related to primary window.
                newWindow.setX(0);
                newWindow.setY(0);

                // init chart
                final CategoryAxis DaysAxisX = new CategoryAxis();
                final NumberAxis DurationAxisY = new NumberAxis();
                final BarChart<String,Number> barChart =
                        new BarChart<String,Number>(DaysAxisX,DurationAxisY);
                barChart.setTitle("Activity");
                DaysAxisX.setLabel("Days");
                DurationAxisY.setLabel("Duration");

                XYChart.Series sleepingSeries = new XYChart.Series();
                sleepingSeries.setName("Sleeping");
                XYChart.Series toiletingSeries = new XYChart.Series();
                toiletingSeries.setName("Toileting");
                XYChart.Series showeringSeries = new XYChart.Series();
                showeringSeries.setName("Showering");
                XYChart.Series breakfastSeries = new XYChart.Series();
                breakfastSeries.setName("Breakfast");
                XYChart.Series groomingSeries = new XYChart.Series();
                groomingSeries.setName("Grooming");
                XYChart.Series sparetimeSeries = new XYChart.Series();
                sparetimeSeries.setName("Sparetime");
                XYChart.Series leavingSeries = new XYChart.Series();
                leavingSeries.setName("Leaving");
                XYChart.Series lunchSeries = new XYChart.Series();
                lunchSeries.setName("Lunch");
                XYChart.Series snackSeries = new XYChart.Series();
                snackSeries.setName("Snack");
                for(Activity a: activities){
                    switch (a.getName()){
                        case "Sleeping\t\t":
                            sleepingSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            if(a.getDuration()>=720) warning = 1;
                            break;
                        case "Toileting\t":
                            toiletingSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            if(a.getDuration()>=60) warning = 1;
                            break;
                        case "Showering\t":
                            showeringSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            if(a.getDuration()>=60) warning = 1;
                            break;
                        case "Breakfast\t":
                            breakfastSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            break;
                        case "Grooming\t":
                            groomingSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            if(a.getDuration()>=60) warning = 1;
                            break;
                        case "Spare_Time/TV\t\t":
                            sparetimeSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            break;
                        case "Leaving\t":
                            leavingSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            if(a.getDuration()>=720) warning = 1;
                            break;
                        case "Lunch\t\t":
                            lunchSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            break;
                        case "Snack\t":
                            snackSeries.getData().add(new XYChart.Data(""+ a.getDate(), a.getDuration()));
                            break;
                    }
                }
                barChart.getData().addAll(sleepingSeries,toiletingSeries,showeringSeries,breakfastSeries,groomingSeries,sparetimeSeries,leavingSeries,lunchSeries,snackSeries);
                String clicked = lv.getSelectionModel().getSelectedItem();
                int id = Integer.parseInt(clicked.substring(8,9));
                System.out.println(clicked);
                List<Prescription> prescriptions = patients.get(id-1).getPrescriptionList();
                List<String> prescriptionsInfo = new ArrayList<>();
                for(Prescription p : prescriptions){
                    prescriptionsInfo.add(p.getTaken() + " " + p.getIntake() + " " + p.getMedication());
                }
                ObservableList<String> items = FXCollections.observableArrayList();
                items.addAll(prescriptionsInfo);
                ListView<String> lv = new ListView<String>(items);
//                secondaryLayout.getChildren().addAll(paneChart, paneMed);
                secondaryLayout.setCenter(barChart);
                secondaryLayout.setRight(lv);
                if(warning==1) {
                    warningLabel.setText("Something is wrong with patient. Check Now!");
                    warningLabel.setTextFill(Color.web("#FF0000"));
                }
                BorderPane.setAlignment(warningLabel, Pos.CENTER);
                secondaryLayout.setTop(warningLabel);
                newWindow.show();
            }
        });
        lv.setPrefHeight(640);
        lv.setPrefWidth(480);
        pane1.getChildren().add(lv);

        rootPane.getChildren().addAll(pane1);
        stage.show();
    }
}
