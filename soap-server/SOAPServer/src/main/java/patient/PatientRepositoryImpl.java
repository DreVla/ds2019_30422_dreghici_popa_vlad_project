package patient;

import java.util.ArrayList;
import java.util.List;

public class PatientRepositoryImpl implements PatientRepository{

    private List<Patient> patientList;

    public PatientRepositoryImpl() {
        patientList = new ArrayList<>();
        loadPatients();

    }

    public List<Patient> getAllPatients() {
        return patientList;
    }

    public Patient getPatient(int id) {
        for (Patient emp : patientList) {
            if (emp.getId() == id) {
                return emp;
            }
        }
        return null;
    }

    private void loadPatients() {

        Prescription p1 = new Prescription();
        p1.setId(1);
        p1.setIntake("100mg");
        p1.setMedication("Algocalmin");
        p1.setTaken("Taken");

        Prescription p2 = new Prescription();
        p2.setId(1);
        p2.setIntake("200mg");
        p2.setMedication("Strepsils");
        p2.setTaken("Not Taken");

        Prescription p3 = new Prescription();
        p3.setId(1);
        p3.setIntake("50mg");
        p3.setMedication("Brufen");
        p3.setTaken("Not Taken");

        Prescription p4 = new Prescription();
        p4.setId(1);
        p4.setIntake("100mg");
        p4.setMedication("Faringosept");
        p4.setTaken("Taken");

        Prescription p5 = new Prescription();
        p5.setId(1);
        p5.setIntake("100mg");
        p5.setMedication("Ospamox");
        p5.setTaken("Taken");

        Prescription p6 = new Prescription();
        p6.setId(1);
        p6.setIntake("100mg");
        p6.setMedication("MedicationNew");
        p6.setTaken("Not Taken");

        List<Prescription> pl1 = new ArrayList<>();
        pl1.add(p1);
        pl1.add(p2);
        pl1.add(p3);
        pl1.add(p5);
        pl1.add(p4);
        pl1.add(p6);

        List<Prescription> pl2 = new ArrayList<>();
        pl2.add(p4);
        pl2.add(p2);
        pl2.add(p5);
        pl2.add(p6);

        List<Prescription> pl3 = new ArrayList<>();
        pl3.add(p1);
        pl3.add(p5);
        pl3.add(p3);
        pl3.add(p6);

        patientList.add(new Patient(1, "Patient 1", pl1));
        patientList.add(new Patient(2, "Patient 2", pl2));
        patientList.add(new Patient(3, "Patient 3", pl3));
    }
}
