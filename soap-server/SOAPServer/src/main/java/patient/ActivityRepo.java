package patient;

import java.util.List;

public interface ActivityRepo {

    List<Activity> getAllActivities();
}