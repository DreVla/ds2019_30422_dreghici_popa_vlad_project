package patient;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.List;

@WebService
public class PatientServiceImpl implements PatientService {

    @Inject
    private PatientRepositoryImpl patientRepository;

    public PatientServiceImpl() {
        this.patientRepository = new PatientRepositoryImpl();
    }

    @Override
    public Patient getPatient(int id) {
        return patientRepository.getPatient(id);
    }

    @Override
    public List<Patient> getAllPatients() {
        return patientRepository.getAllPatients();
    }
}
