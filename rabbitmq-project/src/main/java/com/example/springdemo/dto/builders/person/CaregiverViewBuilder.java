package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.CaregiverDTO;
import com.example.springdemo.dto.person.CaregiverViewDTO;
import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.DoctorViewDTO;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Doctor;

public class CaregiverViewBuilder {

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver) {
        return new CaregiverViewDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getRole(),
                caregiver.getBirthdate(),
                caregiver.getPatients());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO) {
        return new Caregiver(
                caregiverViewDTO.getId(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getPatients());
    }

    public static CaregiverDTO generateDTOwithPass(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getRole(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getPatients());
    }
}
