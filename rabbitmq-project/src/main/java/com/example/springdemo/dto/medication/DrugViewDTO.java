package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Prescription;

public class DrugViewDTO {

    private Integer id;
    private String name;
    private String sideeffects;
    private Prescription prescription;

    public DrugViewDTO(Integer id, String name, String sideeffects, Prescription prescription) {
        this.id = id;
        this.name = name;
        this.sideeffects = sideeffects;
        this.prescription = prescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffects() {
        return sideeffects;
    }

    public void setSideeffects(String sideeffects) {
        this.sideeffects = sideeffects;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }
}