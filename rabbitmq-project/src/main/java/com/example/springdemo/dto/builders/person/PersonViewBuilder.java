package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.PersonViewDTO;
import com.example.springdemo.entities.person.Person;

public class PersonViewBuilder {
    public static PersonViewDTO generateDTOFromEntity(Person person){
        return new PersonViewDTO(
                person.getId(),
                person.getName(),
                person.getRole());
    }

    public static Person generateEntityFromDTO(PersonViewDTO personViewDTO){
        return new Person(
                personViewDTO.getId(),
                personViewDTO.getName(),
                personViewDTO.getRole());
    }
}
