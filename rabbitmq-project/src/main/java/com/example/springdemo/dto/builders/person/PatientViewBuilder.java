package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.DoctorViewDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.entities.person.Patient;

public class PatientViewBuilder {
    public static PatientViewDTO generateDTOFromEntity(Patient patient) {
        return new PatientViewDTO(
                patient.getId(),
                patient.getName(),
                patient.getGender(),
                patient.getRole(),
                patient.getBirthdate(),
                patient.getCareGiver(),
                patient.getMedicationPlan());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientViewDTO) {
        return new Patient(
                patientViewDTO.getId(),
                patientViewDTO.getName(),
                patientViewDTO.getCareGiver(),
                patientViewDTO.getMedicationPlan());
    }

    public static PatientDTO generateDTOwithPass(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getRole(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getCareGiver(),
                patient.getMedicationPlan());
    }
}
