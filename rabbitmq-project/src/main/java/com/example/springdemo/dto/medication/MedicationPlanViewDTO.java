package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Prescription;
import com.example.springdemo.entities.person.Patient;

import java.util.Set;

public class MedicationPlanViewDTO {

    private Integer id;
    private Set<Prescription> prescriptionSet;
    private Patient patient;

    public MedicationPlanViewDTO(Integer id, Set<Prescription> prescriptionSet, Patient patient) {
        this.id = id;
        this.prescriptionSet = prescriptionSet;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Prescription> getPrescriptionSet() {
        return prescriptionSet;
    }

    public void setPrescriptionSet(Set<Prescription> prescriptionSet) {
        this.prescriptionSet = prescriptionSet;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
