package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.person.Patient;

public class DrugViewBuilder {
    public static DrugViewDTO generateDTOFromEntity(Drug drug) {
        return new DrugViewDTO(
                drug.getId(),
                drug.getName(),
                drug.getSideeffects(),
                drug.getPrescription());

    }

    public static Drug generateEntityFromDTO(DrugViewDTO drugViewDTO) {
        return new Drug(
                drugViewDTO.getId(),
                drugViewDTO.getName(),
                drugViewDTO.getSideeffects(),
                drugViewDTO.getPrescription());
    }
}
