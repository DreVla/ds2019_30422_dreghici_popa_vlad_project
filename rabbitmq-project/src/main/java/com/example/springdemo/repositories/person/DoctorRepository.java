package com.example.springdemo.repositories.person;

import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.entities.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.print.Doc;
import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Query(value = "SELECT u " + "FROM Doctor u " + "ORDER BY u.name")
    List<Doctor> getAllOrdered();

    @Query(value = "SELECT d FROM Doctor d WHERE d.username = :username")
    Doctor getDoctorByUsername(@Param("username")String username);
}
