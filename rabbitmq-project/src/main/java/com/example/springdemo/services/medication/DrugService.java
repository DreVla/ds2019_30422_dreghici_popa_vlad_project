package com.example.springdemo.services.medication;

import com.example.springdemo.dto.builders.medication.DrugBuilder;
import com.example.springdemo.dto.builders.medication.DrugViewBuilder;
import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.medication.DrugRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DrugService {

    private final DrugRepository drugRepository;

    @Autowired
    public DrugService(DrugRepository drugRepository) {
        this.drugRepository = drugRepository;
    }

    public DrugViewDTO findUserById(Integer id) {
        Optional<Drug> drug = drugRepository.findById(id);

        if (!drug.isPresent()) {
            throw new ResourceNotFoundException("Drug", "user id", id);
        }
        return DrugViewBuilder.generateDTOFromEntity(drug.get());
    }

    public List<DrugViewDTO> findAll() {
        List<Drug> drugs = drugRepository.getAllOrdered();

        return drugs.stream()
                .map(DrugViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DrugDTO drugDTO) {

        PersonFieldValidator.validateInsertOrUpdateDrug(drugDTO);

        return drugRepository
                .save(DrugBuilder.generateEntityFromDTO(drugDTO))
                .getId();
    }

    public Integer update(DrugDTO drugDTO) {

        Optional<Drug> drug = drugRepository.findById(drugDTO.getId());

        if (!drug.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", drugDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdateDrug(drugDTO);

        return drugRepository.save(DrugBuilder.generateEntityFromDTO(drugDTO)).getId();
    }

    public void delete(DrugViewDTO drugViewDTO) {
        this.drugRepository.deleteById(drugViewDTO.getId());
    }
}
