package com.example.springdemo.validators;

import com.example.springdemo.dto.activity.ActivityDTO;
import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.dto.medication.PrescriptionDTO;
import com.example.springdemo.dto.person.CaregiverDTO;
import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.PersonDTO;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class PersonFieldValidator{

    private static final Log LOGGER = LogFactory.getLog(PersonFieldValidator.class);
    private static final EmailFieldValidator EMAIL_VALIDATOR = new EmailFieldValidator() ;

    public static void validateInsertOrUpdateDoctor(DoctorDTO doctorDTO) {

        List<String> errors = new ArrayList<>();
        if (doctorDTO == null) {
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertOrUpdateCaregiver(CaregiverDTO caregiverDTO) {
        List<String> errors = new ArrayList<>();
        if (caregiverDTO == null) {
            errors.add("caregiverDTO is null");
            throw new IncorrectParameterException(CaregiverDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertOrUpdatePatient(PatientDTO patientDTO) {
        List<String> errors = new ArrayList<>();
        if (patientDTO == null) {
            errors.add("patientDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertOrUpdateDrug(DrugDTO drugDTO) {
        List<String> errors = new ArrayList<>();
        if (drugDTO == null) {
            errors.add("drugDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertOrUpdatePrescription(PrescriptionDTO prescriptionDTO) {
        List<String> errors = new ArrayList<>();
        if (prescriptionDTO == null) {
            errors.add("prescriptionDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateInsertOrUpdateMedicationPlan(MedicationPlanDTO medicationPlanDTO) {
        List<String> errors = new ArrayList<>();
        if (medicationPlanDTO == null) {
            errors.add("medicationPlanDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }

    }

    public static void validateInsertOrUpdateActivity(ActivityDTO activityDTO) {
        List<String> errors = new ArrayList<>();
        if (activityDTO == null) {
            errors.add("drugDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(PersonFieldValidator.class.getSimpleName(), errors);
        }
    }
}
