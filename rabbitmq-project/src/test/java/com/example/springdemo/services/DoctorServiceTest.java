package com.example.springdemo.services;

import com.example.springdemo.SpringDemoApplicationTests;
import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.DoctorViewDTO;
import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;
import com.example.springdemo.errorhandler.IncorrectParameterException;
import com.example.springdemo.services.person.DoctorService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class DoctorServiceTest extends SpringDemoApplicationTests {

    @Autowired
    DoctorService doctorService;


    @Test(expected = IncorrectParameterException.class)
    public void insertDTOBad() {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setBirthdate("23.1.1993");
        doctorService.insert(doctorDTO);

    }
    @Test
    public void insertDTOGood() {
        DoctorDTO doctorDTO = new DoctorDTO();
        doctorDTO.setName("John Patterson");
        doctorDTO.setBirthdate("23.04.2000");
        doctorDTO.setGender(Gender.MALE);
        doctorDTO.setRole(Role.DOCTOR);
        Integer id = doctorService.insert(doctorDTO);
        DoctorViewDTO person2 = doctorService.findUserById(id);
        assert(!doctorDTO.equals(person2));

    }
}
