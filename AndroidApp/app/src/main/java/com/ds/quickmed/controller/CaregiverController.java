package com.ds.quickmed.controller;

import android.content.Context;
import android.util.Log;

import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.CaregiverService;
import com.ds.quickmed.utils.CaregiverCurrentPatientsRVAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaregiverController {


    public void getAllPatientsForCaregiver(int id, final Context context, final CaregiverCurrentPatientsRVAdapter mAdapter) {
        final Call<List<Patient>> patientsCall = CaregiverService.getInstance().getAllPatientsForCaregiver(id);
        patientsCall.enqueue(new Callback<List<Patient>>() {
            @Override
            public void onResponse(Call<List<Patient>> call, Response<List<Patient>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Patient> patients = response.body();
                    if(patients!=null){
                        mAdapter.replaceAll(patients);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", "BAD");
                }
            }

            @Override
            public void onFailure(Call<List<Patient>> call, Throwable t) {
            }
        });
    }

}
