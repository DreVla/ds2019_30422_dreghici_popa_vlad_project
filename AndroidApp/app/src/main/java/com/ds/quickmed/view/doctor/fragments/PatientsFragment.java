package com.ds.quickmed.view.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.controller.DoctorController;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.utils.PatientsRecyclerViewAdapter;
import com.ds.quickmed.view.doctor.fragments.add.AddPatientActivity;
import com.ds.quickmed.view.doctor.views.ViewPatientActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class PatientsFragment extends Fragment {
    private RecyclerView recyclerView;
    private PatientsRecyclerViewAdapter mAdapter;
    private List<Patient> patients = new ArrayList<>();
    private DoctorController doctorController;
    private FloatingActionButton addPatient;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_patients, viewGroup, false);
        recyclerView = rootView.findViewById(R.id.patients_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new PatientsRecyclerViewAdapter(getActivity(), patients);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        doctorController = new DoctorController();
        doctorController.getAllPatients(getActivity(), mAdapter);
        mAdapter.setClickListener(new PatientsRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent patientPage = new Intent(getActivity(), ViewPatientActivity.class);
                patientPage.putExtra("id", mAdapter.getItem(position).getId());
                patientPage.putExtra("obj", mAdapter.getItem(position));
                startActivity(patientPage);
            }

            @Override
            public void onRemoveClick(View view, int position) {
                doctorController.deletePatient(getActivity(), mAdapter.getItem(position), mAdapter);
                doctorController.getAllPatients(getActivity(),mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });

        addPatient = rootView.findViewById(R.id.fab_add_patient);
        addPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNewPatient();
            }
        });
        return rootView;
    }


    public void openAddNewPatient(){
        Intent intent = new Intent(getActivity(), AddPatientActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        doctorController.getAllPatients(getActivity(), mAdapter);
    }
}