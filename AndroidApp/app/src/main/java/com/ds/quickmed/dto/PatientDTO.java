package com.ds.quickmed.dto;



import com.ds.quickmed.model.enums.*;
import com.ds.quickmed.model.medication.MedicationPlan;
import com.ds.quickmed.model.person.Caregiver;

import java.util.Objects;

public class PatientDTO {

    private Integer id;
    private String name;
    private String birthdate;
    private Gender gender;
    private Role role;
    private String username;
    private String password;
    private Caregiver careGiver;
    private MedicationPlan medicationPlan;

    public PatientDTO() {
    }

    public PatientDTO(Integer id, String name, String birthdate, Gender gender, Role role, String username, String password, Caregiver careGiver, MedicationPlan medicationPlan) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.role = role;
        this.username = username;
        this.password = password;
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Caregiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(Caregiver careGiver) {
        this.careGiver = careGiver;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthdate, that.birthdate) &&
                gender == that.gender &&
                role == that.role &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(careGiver, that.careGiver) &&
                Objects.equals(medicationPlan, that.medicationPlan);
    }

}
