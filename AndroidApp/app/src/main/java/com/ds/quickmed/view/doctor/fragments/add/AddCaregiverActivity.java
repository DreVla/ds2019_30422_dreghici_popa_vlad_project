package com.ds.quickmed.view.doctor.fragments.add;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.ds.quickmed.R;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.service.CaregiverService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCaregiverActivity extends AppCompatActivity {

    private EditText username, password, name, birthdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_caregiver);

        username = findViewById(R.id.new_caregiver_username_edittext);
        password = findViewById(R.id.new_caregiver_password_edittext);
        name = findViewById(R.id.new_caregiver_name_edittext);
        birthdate = findViewById(R.id.new_caregiver_birthdate_edittext);
    }

    public void sendAddCaregiver(View view) {
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();
        String name = this.name.getText().toString();
        String birthdate = this.birthdate.getText().toString();
        Caregiver caregiver = new Caregiver();
        caregiver.setUsername(username);
        caregiver.setPassword(password);
        caregiver.setName(name);
        caregiver.setBirthdate(birthdate);
        final Call<ResponseBody> sendPost = CaregiverService.getInstance().addNewCaregiver(caregiver);
        sendPost.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    setResult(RESULT_OK);
                    finish();
                } else {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
            }
        });
    }
}
