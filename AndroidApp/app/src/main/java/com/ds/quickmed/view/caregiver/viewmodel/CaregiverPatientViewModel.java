package com.ds.quickmed.view.caregiver.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.PatientService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class CaregiverPatientViewModel extends ViewModel {


    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<String> birthdate = new MutableLiveData<>();
    public MutableLiveData<String> gender = new MutableLiveData<>();

    public void findById(int patientId){
        final Call<Patient> patientCall = PatientService.getInstance().getPatientById(patientId);

        patientCall.enqueue(new Callback<Patient>() {
            @Override
            public void onResponse(Call<Patient> call, Response<Patient> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: " + response.body().getName());
                    Patient patient = response.body();
                    name.setValue(patient.getName());
                    birthdate.setValue(patient.getBirthdate());
                    gender.setValue(patient.getGender().toString());
                } else{
                    Log.d(TAG, "onResponse: smth went wrong");
                }

            }

            @Override
            public void onFailure(Call<Patient> call, Throwable t) {
                Log.d(TAG, "onFailure: " +t.getCause());
            }
        });
    }
}
