package com.ds.quickmed.model.person;

import com.ds.quickmed.model.enums.Gender;
import com.ds.quickmed.model.enums.Role;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Set;

public class Caregiver extends Person  implements Serializable {

    @SerializedName("patients")
    @Expose
    private Set<Patient> patients;

    public Caregiver() {
    }

    public Caregiver(Integer id, String name, Set<Patient> patients) {
        super(id, name, Role.CAREGIVER);
        this.patients = patients;
    }

    public Caregiver(Integer id, String name, String birthdate, Gender gender, String username, String password, Set<Patient> patients) {
        super(id, name, birthdate, gender, Role.CAREGIVER, username, password);
        this.patients = patients;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }

    public void addPatient(Patient patient){
        this.patients.add(patient);
    }
}
