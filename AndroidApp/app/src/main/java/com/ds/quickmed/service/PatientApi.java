package com.ds.quickmed.service;

import com.ds.quickmed.model.person.Patient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PatientApi {

    @GET("/patients/{id}")
    Call<Patient> getPatientById(@Path("id") Integer var);

    @GET("/patients/username/{username}")
    Call<Patient> getPatientByUsername(@Path("username") String username);

    @Headers({"Accept: application/json"})
    @POST("patients/insert/")
    Call<ResponseBody> addNewPatient(@Body Patient patient);

    @HTTP(method = "DELETE", path = "patients/delete", hasBody = true)
    Call<ResponseBody> deletePatient(@Body Patient patient);

    @Headers({"Accept: application/json"})
    @PUT("patients/update/")
    Call<ResponseBody> updatePatient(@Body Patient patient);




}

