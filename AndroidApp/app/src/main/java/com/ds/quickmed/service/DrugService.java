package com.ds.quickmed.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DrugService {

//    private final static String API_URL = "http://10.0.2.2:8080/";

//    private final static String API_URL = "http://192.168.43.136:8080/"; // hotspot

    private final static String API_URL = "http://192.168.0.103:8080/"; // home

    private static DrugApi drugApi;

    //get la instanta de retrofit
    public static DrugApi getInstance() {
        if (drugApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            drugApi = retrofit.create(DrugApi.class);
        }
        return drugApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }
}
