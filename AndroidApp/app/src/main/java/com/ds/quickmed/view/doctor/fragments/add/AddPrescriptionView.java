package com.ds.quickmed.view.doctor.fragments.add;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.ds.quickmed.R;
import com.ds.quickmed.databinding.ActivityAddPrescriptionViewBinding;
import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.model.medication.Prescription;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.DoctorService;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPrescriptionView extends AppCompatActivity {

    private AddPrescriptionViewModel viewModel;
    private ActivityAddPrescriptionViewBinding binding;
    private List<Drug> spinnerDrugs = new ArrayList<>();
    private ArrayAdapter<Drug> adapter;
    private EditText intakeEditText;
    private Spinner sItems;
    private Patient patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_prescription_view);
        viewModel = ViewModelProviders.of(this).get(AddPrescriptionViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setViewModelAddPrescription(viewModel);

        viewModel.getAllDrugs();

        patient = (Patient) getIntent().getExtras().getSerializable("obj");
        sItems = findViewById(R.id.drugs_spinner);
        intakeEditText = findViewById(R.id.intake_edittext);
        observeSpinner();
    }

    private void observeSpinner(){
        viewModel.drugsLiveData.observe(this, new Observer<List<Drug>>() {
            @Override
            public void onChanged(List<Drug> drugs) {
                adapter = new ArrayAdapter<Drug>(
                        getApplicationContext(), android.R.layout.simple_spinner_item, drugs);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                sItems.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void addPrescriptionToPlan(View view) {
        Drug drugToAdd = (Drug) sItems.getSelectedItem();
        String intake = intakeEditText.getText().toString();
        Prescription prescription = new Prescription();
        prescription.setDrug(drugToAdd);
        prescription.setIntake(intake);
        prescription.setMedicationPlan(patient.getMedicationPlan());
        final Call<ResponseBody> sendPost = DoctorService.getInstance().addPrescription(prescription);
        sendPost.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    setResult(RESULT_OK);
                    finish();
                } else {
                    setResult(RESULT_CANCELED);
                    Toast.makeText(AddPrescriptionView.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Wrong", "onFailure: Smth wrong");
                Toast.makeText(AddPrescriptionView.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
