package com.ds.quickmed.view.caregiver;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.controller.CaregiverController;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.utils.CaregiverCurrentPatientsRVAdapter;
import com.ds.quickmed.view.caregiver.view.CaregiverPatientActivity;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MainCaregiverActivity extends AppCompatActivity {

    private Caregiver caregiver;
    private List<Patient> patients = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_caregiver);

        Bundle b = getIntent().getExtras();
        caregiver = (Caregiver) b.getSerializable("obj");

        RecyclerView patientsRecyclerView = findViewById(R.id.caregiver_patients_recycler_view);
        patientsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        final CaregiverCurrentPatientsRVAdapter mAdapter = new CaregiverCurrentPatientsRVAdapter(getApplicationContext(), patients);
        patientsRecyclerView.setAdapter(mAdapter);
        patientsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        CaregiverController caregiverController = new CaregiverController();
        caregiverController.getAllPatientsForCaregiver(caregiver.getId(), getApplicationContext(), mAdapter);
        mAdapter.setClickListener(new CaregiverCurrentPatientsRVAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), CaregiverPatientActivity.class);
                Log.d(TAG, "onItemClick: " + mAdapter.getItem(position).getId());
                intent.putExtra("id", mAdapter.getItem(position).getId());
                intent.putExtra("obj",mAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_out:
                //TODO logout
//                logOut();
                break;
            case R.id.refresh:
                finish();
                startActivity(getIntent());
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
