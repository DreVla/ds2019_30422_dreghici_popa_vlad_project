package com.ds.quickmed.view.doctor.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.service.DrugService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ViewDrugViewModel extends ViewModel {
    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<String> sideeffects = new MutableLiveData<>();

    public void findById(int drugId){
        final Call<Drug> drugCall = DrugService.getInstance().getDrugById(drugId);

        drugCall.enqueue(new Callback<Drug>() {
            @Override
            public void onResponse(Call<Drug> call, Response<Drug> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: " + response.body().getName());
                    Drug drug = response.body();
                    name.setValue(drug.getName());
                    sideeffects.setValue(drug.getSideeffects());
                } else{
                    Log.d(TAG, "onResponse: smth went wrong");
                }

            }

            @Override
            public void onFailure(Call<Drug> call, Throwable t) {
                Log.d(TAG, "onFailure: " +t.getCause());
            }
        });
    }

    public void sendPut(Drug drug, String name, String sideeffects){
        drug.setName(name);
        drug.setSideeffects(sideeffects);
        final Call<ResponseBody> call = DrugService.getInstance().updateDrug(drug);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse: Success");
                } else {
                    Log.d(TAG, "onResponse: Failure");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getCause());
            }
        });
    }
}
