package com.ds.quickmed.view.doctor.views;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.ds.quickmed.R;
import com.ds.quickmed.databinding.ActivityViewDrugBinding;
import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.view.doctor.viewmodel.ViewDrugViewModel;

public class ViewDrugActivity extends AppCompatActivity {

    private static final String TAG = "Found";
    private ViewDrugViewModel viewDrugViewModel;
    private ActivityViewDrugBinding binding;
    private EditText name, sideeffects;
    private Drug drug;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_drug);
        viewDrugViewModel = ViewModelProviders.of(this).get(ViewDrugViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setViewDrugViewModel(viewDrugViewModel);

        name = findViewById(R.id.drug_page_name);
        sideeffects = findViewById(R.id.drug_page_sideeffects);

        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        drug = (Drug) b.getSerializable("obj");
        viewDrugViewModel.findById(id);
        setEditTexts(false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details_edit_button:
                setEditTexts(true);
                break;
            case R.id.details_save_button:
                viewDrugViewModel.sendPut(drug, name.getText().toString(), sideeffects.getText().toString());
                setEditTexts(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setEditTexts(boolean status) {
        name.setEnabled(status);
        sideeffects.setEnabled(status);
    }

}
