package com.ds.quickmed.view.doctor.views;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.controller.CaregiverController;
import com.ds.quickmed.databinding.ActivityViewCaregiverBinding;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.utils.CaregiverCurrentPatientsRVAdapter;
import com.ds.quickmed.view.doctor.viewmodel.ViewCaregiverViewModel;

import java.util.ArrayList;
import java.util.List;

public class ViewCaregiverActivity extends AppCompatActivity {

    private static final String TAG = "Found";
    private ViewCaregiverViewModel caregiverViewModel;
    private ActivityViewCaregiverBinding binding;
    private EditText name, birthdate;
    private Caregiver caregiver;

    private List<Patient> patients = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_caregiver);
        caregiverViewModel = ViewModelProviders.of(this).get(ViewCaregiverViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setViewCaregiverViewModel(caregiverViewModel);

        name = findViewById(R.id.caregiver_page_name);
        birthdate = findViewById(R.id.caregiver_page_birthdate);

        Bundle b = getIntent().getExtras();
        int id = b.getInt("id");
        caregiver = (Caregiver) b.getSerializable("obj");
        caregiverViewModel.findById(id);
        RecyclerView patientsRecyclerView = findViewById(R.id.doctor_caregiver_patients_rv);
        patientsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        final CaregiverCurrentPatientsRVAdapter mAdapter = new CaregiverCurrentPatientsRVAdapter(getApplicationContext(), patients);
        patientsRecyclerView.setAdapter(mAdapter);
        patientsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        CaregiverController caregiverController = new CaregiverController();
        caregiverController.getAllPatientsForCaregiver(id, getApplicationContext(), mAdapter);
        setEditTexts(false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details_edit_button:
                setEditTexts(true);
                break;
            case R.id.details_save_button:
                caregiverViewModel.sendPut(caregiver, name.getText().toString(), birthdate.getText().toString());
                setEditTexts(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setEditTexts(boolean status) {
        name.setEnabled(status);
        birthdate.setEnabled(status);
    }

}
