package com.example.springdemo.controller.medication;


import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.dto.medication.MedicationPlanViewDTO;
import com.example.springdemo.dto.medication.PrescriptionViewDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.services.medication.MedicationPlanService;
import com.example.springdemo.services.medication.PrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationplans")
public class MedicationPlanController {

    @Autowired
    private MedicationPlanService medicationPlanService;
    @Autowired
    private PrescriptionService prescriptionService;

    public MedicationPlanController(MedicationPlanService medicationPlanService, PrescriptionService prescriptionService) {
        this.medicationPlanService = medicationPlanService;
        this.prescriptionService = prescriptionService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<MedicationPlanViewDTO> findAllMedicationPlans(){
        return medicationPlanService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public MedicationPlanViewDTO findMedicationPlanById(@PathVariable("id") Integer id){
        return medicationPlanService.findUserById(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updateMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        medicationPlanService.update(medicationPlanDTO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteMedicationPlanDTO(@RequestBody MedicationPlanViewDTO medicationPlanViewDTO){
        medicationPlanService.delete(medicationPlanViewDTO);
    }

    @RequestMapping(value = "/{id}/prescriptions", method = RequestMethod.GET)
    public List<PrescriptionViewDTO> findAllPatients(@PathVariable("id") Integer id){
        return prescriptionService.findAllForMedicationPlan(id);
    }
}
