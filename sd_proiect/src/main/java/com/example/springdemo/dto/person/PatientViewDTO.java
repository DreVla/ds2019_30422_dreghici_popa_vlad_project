package com.example.springdemo.dto.person;

import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.entities.person.Caregiver;

public class PatientViewDTO {

    private Integer id;
    private String name;
    private Gender gender;
    private Role role;
    private String birthdate;
    private Caregiver careGiver;
    private MedicationPlan medicationPlan;

    public PatientViewDTO(Integer id, String name,Gender gender, Role role, String birthdate, Caregiver careGiver, MedicationPlan medicationPlan) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.role = role;
        this.birthdate = birthdate;
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Caregiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(Caregiver careGiver) {
        this.careGiver = careGiver;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
