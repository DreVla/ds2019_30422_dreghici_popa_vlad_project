package com.example.springdemo.gRPC;

import com.example.springdemo.controller.medication.MedicationPlanController;
import com.example.springdemo.dto.medication.MedicationPlanViewDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.Prescription;
import io.grpc.stub.StreamObserver;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

public class MedicationPlanGRPCService extends medicationPlanGrpc.medicationPlanImplBase {

    @Autowired
    private MedicationPlanController medicationPlanController;

    @Override
    public void displayMedicationPlan(MedPlan.Patient request, StreamObserver<MedPlan.MedicationPlan> responseObserver) {
        System.out.println("Inside display medication method");
        String patientName = request.getPatientName();
        int medicationPlanId = request.getId();

        // here after we receive the user we will have
        // to make a search after id and find the medication plan for him
        // after that, we will have to return the medication plan as
        // response
//        MedicationPlanViewDTO medicationPlanViewDTO = medicationPlanController.findMedicationPlanById(medicationPlanId);
//        MedPlan.MedicationPlan.Builder medicationPlan = MedPlan.MedicationPlan.newBuilder();
//        medicationPlan.setId(medicationPlanId);
//        Set<Prescription> prescriptions = medicationPlanViewDTO.getPrescriptionSet();
//        for(Prescription p: prescriptions){
//            MedPlan.Medication.Builder medication = MedPlan.Medication.newBuilder();
//            Drug drug = p.getDrug();
//            medication.setId(p.getId());
//            medication.setName(drug.getName());
//            medication.setIntake(p.getIntake());
//            medicationPlan.addMedicationList(medication);
//        }
//
//        responseObserver.onNext(medicationPlan.build());
//        responseObserver.onCompleted();

        MedPlan.Medication.Builder medication = MedPlan.Medication.newBuilder();
        medication.setId(1);
        medication.setName("Algocalmin");
        medication.setIntake("1/day");
        MedPlan.Medication.Builder medication2 = MedPlan.Medication.newBuilder();
        medication2.setId(2);
        medication2.setName("Bromazepam");
        medication2.setIntake("2/day");

        MedPlan.MedicationPlan.Builder medicationPlan = MedPlan.MedicationPlan.newBuilder();
        medicationPlan.setId(1);
        medicationPlan.addMedicationList(medication);
        medicationPlan.addMedicationList(medication2);

        responseObserver.onNext(medicationPlan.build());
        responseObserver.onCompleted();

    }
}