package com.example.springdemo.entities.medication;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "DRUG")
public class Drug {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "sideeffects", length = 200)
    private String sideeffects;

    @JsonIgnore
    @OneToOne(mappedBy = "drug", cascade = CascadeType.ALL, orphanRemoval = true)
    private Prescription prescription;

    public Drug() {
    }

    public Drug(Integer id, String name, String sideeffects, Prescription prescription) {
        this.id = id;
        this.name = name;
        this.sideeffects = sideeffects;
        this.prescription = prescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffects() {
        return sideeffects;
    }

    public void setSideeffects(String sideeffects) {
        this.sideeffects = sideeffects;
    }

    public Prescription getPrescription() {
        return prescription;
    }

    public void setPrescription(Prescription prescription) {
        this.prescription = prescription;
    }
}
