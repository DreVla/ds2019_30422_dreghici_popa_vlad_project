package com.example.springdemo.entities.enums;

public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
