package com.example.springdemo.services.person;

import com.example.springdemo.dto.builders.person.DoctorBuilder;
import com.example.springdemo.dto.builders.person.DoctorViewBuilder;
import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.DoctorViewDTO;
import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.person.DoctorRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorViewDTO findUserById(Integer id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Doctor", "user id", id);
        }
        return DoctorViewBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorViewDTO> findAll() {
        List<Doctor> doctors = doctorRepository.getAllOrdered();

        return doctors.stream()
                .map(DoctorViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {

        PersonFieldValidator.validateInsertOrUpdateDoctor(doctorDTO);

        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Integer update(DoctorDTO doctorDTO) {

        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getId());

        if (!doctor.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", doctorDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdateDoctor(doctorDTO);

        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }

    public void delete(DoctorViewDTO doctorViewDTO) {
        this.doctorRepository.deleteById(doctorViewDTO.getId());
    }

    public DoctorDTO getDoctorByUsername(String username){
        Doctor doctorFound = doctorRepository.getDoctorByUsername(username);
        if(doctorFound != null)
            return DoctorViewBuilder.generateDTOwithPass(doctorFound);
        else
            return null;
    }

}
